# GameBoy Monorepo

## Run

```bash
cd hello-world\
.\Make.bat
.\..\bgb\bgb64.exe hello.gb

.\..\bgb\bgb64.exe --watch hello.gb
```

## Setup

https://blog.flozz.fr/2019/09/18/developpement-gameboy-1bis-re-hello-world/

### Installer SDCC

```bash
sudo apt install build-essential sdcc sdcc-libraries
```

https://sourceforge.net/projects/sdcc/files/sdcc-win64/
Sélectionnez « Full » comme type d'installation, et vérifiez bien que la case « SDCC GBZ80 library » est cochée.
À la fin de l'installation, assurez-vous que la case « Add C:\... to the PATH » soit bien cochée

### compiler gbdk-n (submodule)

```bash
cd gbdk-n
make
```

```cmd
cd gbdk-n
.\Make.bat
```

### Emulateur

http://bgb.bircd.org/#downloads
