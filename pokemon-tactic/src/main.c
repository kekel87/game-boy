#include <gb/gb.h>

#include "battle.tileset.h"
#include "battle.tilemap.h"

void main(void) {
    UINT8 keys;

    set_bkg_data(0, BATTLE_TILESET_TILE_COUNT, BATTLE_TILESET);
    set_bkg_tiles(0, 0, BATTLE_TILEMAP_WIDTH, BATTLE_TILEMAP_HEIGHT, BATTLE_TILEMAP);

    SHOW_BKG;

    while (1) {
        keys = joypad();

        if (keys & J_UP) scroll_bkg(0, -1);
        if (keys & J_DOWN) scroll_bkg(0, 1);
        if (keys & J_LEFT) scroll_bkg(-1, 0);
        if (keys & J_RIGHT) scroll_bkg(1, 0);

        if (keys & J_START) move_bkg(0, 0);

        wait_vbl_done();
    }
}